import React from "react";
import { shallow } from "enzyme";

import App from "../src/common/App/App";
import Property from "../src/common/App/Property";

describe("components", () => {
  describe("App", () => {
    const wrapper = shallow(<App />);

    it("should have all its subcomponents", () => {
      expect(wrapper.find("div .properties-grid").exists()).toBe(true);
      expect(wrapper.find("div .header").exists()).toBe(true);
    });
  });

  describe("Property", () => {
    const wrapper = shallow(<Property />);

    it("should have all its subcomponents", () => {
      expect(wrapper.find("div .property").exists()).toBe(true);
      expect(wrapper.find("div .owner").exists()).toBe(true);
      expect(wrapper.find("div .address").exists()).toBe(true);
      expect(wrapper.find("div .incomeGenerated").exists()).toBe(true);
    });

    it("should display data correctly", () => {
      const repo = {
        data: {
          owner: "foobar",
          line1: "addline1",
          line4: "addline4",
          postcode: "XX-XXX",
          city: "City X",
          country: "Country X",
          incomegenerated: "1",
        },
      };
      wrapper.setProps({ ...repo });

      expect(wrapper.find("div .owner").text()).toEqual("foobar");
      expect(wrapper.find("div .address-line1").text()).toEqual("addline1");
      expect(wrapper.find("div .address-line4").text()).toEqual("addline4");
      expect(wrapper.find("div .address-postcode").text()).toEqual("XX-XXX");
      expect(wrapper.find("div .address-city").text()).toEqual("City X");
      expect(wrapper.find("div .address-country").text()).toEqual("Country X");
      expect(wrapper.find("div .incomeGenerated").text()).toEqual("1£");
    });
  });
});
