DROP Table if exists owners cascade;
Create Table owners (
    id serial primary key,
    name text not null
);

DROP TABLE if exists addresses cascade;
CREATE TABLE addresses (
    id SERIAL PRIMARY KEY,
    line1 TEXT not NULL,
    line2 TEXT,
    line3 TEXT,
    line4 TEXT not NULL,
    postCode TEXT not NULL,
    city TEXT not NULL,
    country TEXT not NULL
);

DROP TABLE if exists properties;
CREATE TABLE properties (
    oid INTEGER,
    aid INTEGER,
    PRIMARY KEY (oid, aid),
    FOREIGN KEY (oid) REFERENCES owners (id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (aid) REFERENCES addresses (id) ON DELETE CASCADE ON UPDATE CASCADE,
    incomeGenerated float not NULL
);

DROP TABLE if exists properties_versions;
CREATE TABLE properties_versions (LIKE properties);
ALTER TABLE properties_versions ADD COLUMN version timestamptz DEFAULT now(), ADD PRIMARY KEY(oid, aid, version);

CREATE OR REPLACE FUNCTION versioning()
  RETURNS TRIGGER AS
$BODY$
BEGIN

    INSERT INTO properties_versions (oid, aid, incomeGenerated) VALUES (NEW.oid, NEW.aid, NEW.incomeGenerated);

    RETURN NULL;
    
END;
$BODY$
  LANGUAGE plpgsql VOLATILE;

CREATE TRIGGER versioning
AFTER INSERT OR UPDATE
ON properties
FOR EACH ROW
EXECUTE PROCEDURE versioning();

INSERT INTO owners (name) VALUES ('carlos');
INSERT INTO owners (name) VALUES ('ankur');
INSERT INTO owners (name) VALUES ('elaine');

INSERT INTO addresses (line1, line4, postCode, city, country) VALUES ('Flat 5', '7 Westbourne Terrace', 'W2 3UL', 'London', 'U.K');
INSERT INTO addresses (line1, line2, line3, line4, postCode, city, country) VALUES ('4', 'Tower Mansions', 'Off Station road', '86-87 Grange Road', 'SE1 3BW', 'London', 'U.K');
INSERT INTO addresses (line1, line2, line4, postCode, city, country) VALUES ('4', '332b', 'Goswell Road',  'EC1V 7LQ', 'London', 'U.K.');

INSERT INTO properties (oid, aid, incomeGenerated) VALUES (1, 1, 2000.34);
INSERT INTO properties (oid, aid, incomeGenerated) VALUES (2, 2, 1000);
INSERT INTO properties (oid, aid, incomeGenerated) VALUES (3, 3, 1200);