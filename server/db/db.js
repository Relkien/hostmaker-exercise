const { Client } = require("pg");

const client = new Client({
  connectionString: "postgresql://relk:123@localhost:5432/hostmaker-sample",
});

client.connect();

module.exports = client;
