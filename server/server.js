require("dotenv").config({ silent: true });

const express = require("express");
const bodyParser = require("body-parser");
const compression = require("compression");
const path = require("path");
const logger = require("./middleware/logger");
const { devMiddleware, hotMiddleware } = require("./middleware/webpack");
const db = require("./db/db.js");

const app = express();

app.set("x-powered-by", false);

app.use(compression());
app.use(logger);

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

if (process.env.NODE_ENV === "production") {
  app.use(express.static("build"));
} else {
  app.use(devMiddleware);
  app.use(hotMiddleware);
}

app.get("/api/properties", (req, res) => {
  db.query(`select owners.name AS owner, 
    addresses.line1 as line1, addresses.line2 as line2, addresses.line3 as line3, addresses.line4 as line4, 
    addresses.postcode as postcode, addresses.city as city, addresses.country as country, incomeGenerated as 
    incomeGenerated from properties inner join owners on properties.oid = owners.id inner join addresses on 
    properties.aid = addresses.id;`, (err, resQ) => {
      if (err) {
        res.sendStatus(400);
      } else {
        res.send(resQ.rows);
      }
    });
});

app.post("/api/properties", (req, res) => {
  const { oid, aid, incomeGenerated } = req.body;
  db.query(`update properties set incomeGenerated = ${incomeGenerated} where oid=${oid} and aid=${aid}`, (err) => {
    if (err) {
      res.sendStatus(400);
    } else {
      res.sendStatus(200);
    }
  });
});

app.get("*", (req, res) => {
  if (process.env.NODE_ENV === "production") {
    res.sendFile(path.resolve("build", "index.html"));
  } else {
    res.write(devMiddleware.fileSystem.readFileSync(path.resolve("build", "index.html")));
    res.end();
  }
});

const server = app.listen(process.env.PORT || 8080, () => {
  console.log("Express started at http://localhost:%d\n", server.address().port);
  if (process.env.NODE_ENV !== "production") {
    console.log("Waiting for webpack...\n");
  }
});
