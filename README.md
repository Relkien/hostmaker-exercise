# Hostmaker exercise

This exercise from Hostmaker was implemented in Node on the Backend and React on the Frontend. A web API architecture was implemented due to the advantage of the flexibility of a API architecture being able to be used by multiple different frontends/scenarios.

A public live deployment is running [here](https://hostmaker-exercise.herokuapp.com/)

## Installation

```
git clone --depth 1 git@bitbucket.org:Relkien/hostmaker-exercise.git project
cd project
cp .env-example .env
npm install or yarn install
```
This exercise requires a database implemented in Postgres.
A SQL script is located at `server/db/script.sql` which you can run to create the tables and set the initial data.

You can go to `server/db/db.js` to configure the connection according to your dev or production environment.

## Usage

```
npm start                   # start server
npm run start:dashboard     # start server using webpack-dashboard
npm run build               # production build (remember to build with NODE_ENV=production)
npm test                    # Runs the tests
```
You can check the app on dev mode by running `npm start` on your machine and going to http://localhost:8080 (default)

To edit the data in the database you can send this curl command for the proof of concept:

`curl -v -H "Content-Type: application/json" -d '{"oid":1, "aid":1, "incomeGenerated":100}' https://localhost:8080/api/properties`