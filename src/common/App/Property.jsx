import React from "react";
import cls from "./App.css";

function Property(props) {
  const { owner, line1, line2, line3, line4, postcode, city, country, incomegenerated }
    = props.data || {};
  return (
    <div className="property">
      <div className="owner">{owner}</div>
      <div className="address">
        <div className="address-line1">{line1}</div>
        { line2 && <div className="address-line2">{line2}</div> }
        { line3 && <div className="address-line3">{line3}</div> }
        <div className="address-line4">{line4}</div>
        <div className="address-postcode">{postcode}</div>
        <div className="address-city">{city}</div>
        <div className="address-country">{country}</div>
      </div>

      <div className="incomeGenerated">{incomegenerated}£</div>
    </div>
  );
}

export default Property;
