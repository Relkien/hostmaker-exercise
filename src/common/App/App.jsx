import React, { Component } from "react";
import axios from "axios";
import cls from "./App.css";

import Property from "./Property";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      title: "Backend code sample - Properties",
      properties: [],
    };

    this.fetchProperties();
  }

  fetchProperties = () => {
    axios.get("/api/properties/").then((response) => {
      this.setState({ properties: response.data });
    });
  }

  render() {
    const { properties, title } = this.state;
    const propertiesDom = properties.map((property, index) =>
      (<Property key={index}
        data={property}
      />));
    return (
      <div>
        <h1 className={cls.title}>{title}</h1>
        <div className="properties-grid">
          <div className="header">
            <div> Owner </div>
            <div> Address </div>
            <div> Income Generated </div>
          </div>
          { propertiesDom }
        </div>
      </div>
    );
  }
}

export default App;
